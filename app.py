from flask import Flask, render_template, request, flash, redirect


app = Flask(__name__)
app.config['SECRET_KEY'] = '7h1515n07453c237k3yd3f1n1731yn07453c237d0n07p4y4773n710n7017'


@app.route('/')
@app.route('/index/', methods = ['GET', 'POST'])
def index():
    with open ('templates/texts/index.txt') as file_index:
        data = file_index.read()
        text_index = []
        for word in data.split():
            text_index.append(word)
    return render_template('index.html', text_index=text_index)


@app.route('/about/', methods = ['GET', 'POST'])
def about():
    return render_template('about.html')


@app.route('/teams/', methods = ['GET', 'POST'])
def teams():
    return render_template('teams.html')

@app.route('/contact/', methods = ['GET', 'POST'])
def contact():
    if request.method == 'POST':
        # theme = request.['theme']
        theme = request.form['theme']
        
        name = request.form['name']
        lastname = request.form['lastname']
        email = request.form['email']
        feedback = request.form['feedback']
        # response = request.form['response']

        


        
        with open('data.txt', 'w', encoding='UTF-8') as data_log:
            data_log.write(f'name = {name}\n'
            f'lastname = {lastname}\n'
            f'email = {email}\n'
            f'theme = {theme}\n'
            f'feedback = {feedback}\n')
        return redirect('/')
    
    return render_template('contact.html')


@app.errorhandler(404)
def error404(e):
    return render_template('404.html'), 404


@app.errorhandler(500)
def error505(e):
    return render_template('500.html'), 500


if __name__ == '__main__':
    app.run()